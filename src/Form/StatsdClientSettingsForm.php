<?php

namespace Drupal\statsd_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Defines a form that configures statsd settings.
 */
class StatsdClientSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'statsd_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'statsd_client.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('statsd_client.settings');
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable StatsD'),
      '#description' => $this->t('Enable StatsD logging. You may want to disable this in non-production environments.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['statsd_server_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Statsd Server'),
      '#collapsible' => TRUE,
    ];

    $form['statsd_server_wrapper']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#size' => 25,
      '#description' => $this->t('The hostname, or IP address of the StatsD daemon. To minimize latency issue, use an IP whenever possible.'),
      '#default_value' => $config->get('host'),
    ];
    $form['statsd_server_wrapper']['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#size' => 5,
      '#description' => $this->t('The port of the StatsD daemon'),
      '#default_value' => $config->get('port'),
    ];

    $form['statsd_server_wrapper']['sample_rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sample Rate'),
      '#size' => 2,
      '#description' => $this->t('StatsD can send a subset of events to Graphite. Choose a lower sample rate if you want to reduce the number of events being sent. Sample rates are between 0 and 1 (e.g. 0.1 implies 10% of events will be logged)'),
      '#default_value' => $config->get('sample_rate'),
    ];

    $form['statsd_server_wrapper']['namespace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Namespace'),
      '#size' => 15,
      '#description' => $this->t('Use a Namespace if you need to separate similar events (such as on different web servers). This namespace is added for calls (if enabled), as well as any calls via the built-in StatsD client. Do not include the period at the end of the Namespace (e.g. use "my.namespace" instead of "my.namespace."'),
      '#default_value' => $config->get('namespace'),
    ];

    $form['events'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Events'),
      '#collapsible' => TRUE,
    ];
    $form['events']['user_events'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send User Events'),
      '#description' => $this->t('Captures various user events in the following categories: active sessions, successful logins, failed logins, page views'),
      '#default_value' => $config->get('events.user_events'),
    ];
    $form['events']['performance_events'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send Performance Events'),
      '#description' => $this->t('Captures various performance events including peak memory usage and page execution time.'),
      '#default_value' => $config->get('events.performance_events'),
    ];
    $form['events']['watchdog_events'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send Watchdog Events'),
      '#description' => $this->t('Captures the severity and type of errors passed through watchdog.'),
      '#default_value' => $config->get('events.watchdog_events'),
    ];
    $form['events']['watchdog_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Log Level'),
      '#description' => $this->t('If watchdog events are enabled, only send data to StatsD at or above the selected threshold'),
      '#options' => RfcLogLevel::getLevels(),
      '#default_value' => $config->get('events.watchdog_level'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('statsd_client.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('host', $form_state->getValue('host'))
      ->set('port', $form_state->getValue('port'))
      ->set('sample_rate', $form_state->getValue('sample_rate'))
      ->set('namespace', $form_state->getValue('namespace'))
      ->set('events.user_events', $form_state->getValue('user_events'))
      ->set('events.performance_events', $form_state->getValue('performance_events'))
      ->set('events.watchdog_events', $form_state->getValue('watchdog_events'))
      ->set('events.watchdog_level', $form_state->getValue('watchdog_level'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('host', trim($form_state->getValue('host')));
    $form_state->setValue('port', trim($form_state->getValue('port')));
    $form_state->setValue('sample_rate', trim($form_state->getValue('sample_rate')));
    $form_state->setValue('namespace', trim(rtrim($form_state->getValue('namespace'), '.')));

    $sample_rate = $form_state->getValue('sample_rate');
    if (!is_numeric($sample_rate) || $sample_rate <= 0 || $sample_rate > 1) {
      $form_state->setErrorByName('sample_rate', t('The sample rate must be a value between 0 and 1'));
    }
  }

}
