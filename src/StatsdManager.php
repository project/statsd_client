<?php

namespace Drupal\statsd_client;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Interface to communicate with Statsd Daemon.
 */
class StatsdManager {

  /**
   * Statsd Hostname.
   *
   * @var string
   */
  protected $host;

  /**
   * Statsd port number.
   *
   * @var int
   */
  protected $port;

  /**
   * Statsd namespace.
   *
   * @var string
   */
  private $namespace = '';

  /**
   * The used UDP socket resource.
   *
   * @var resource|null
   */
  private $socket;

  /**
   * Check if connection is active.
   *
   * @var bool
   */
  private $isConnected = FALSE;

  /**
   * Statsd Client configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Sample Rate.
   *
   * @var float
   */
  private $sampleRateAllMetrics = 1.0;

  /**
   * StatsdManager constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Statsd Client configuration.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->get('statsd_client.settings');
    $this->host = $this->config->get('host');
    $this->port = $this->config->get('port');
    $this->namespace = $this->config->get('namespace');
  }

  /**
   * Increments the key by 1.
   *
   * @param string $key
   *   Bucket name.
   * @param float $sampleRate
   *   Sample Rate.
   * @param array $tags
   *   Tags.
   */
  public function increment(string $key, float $sampleRate = 1.0, array $tags = []): void {
    $this->count($key, 1, $sampleRate, $tags);
  }

  /**
   * Decrements the key by 1.
   *
   * @param string $key
   *   Bucket name.
   * @param float $sampleRate
   *   Sample Rate.
   * @param array $tags
   *   Tags.
   */
  public function decrement(string $key, float $sampleRate = 1.0, array $tags = []): void {
    $this->count($key, -1, $sampleRate, $tags);
  }

  /**
   * Sends a count to statsd.
   *
   * @param string $key
   *   Bucket name.
   * @param int|float $value
   *   Value.
   * @param float $sampleRate
   *   Sample Rate.
   * @param array $tags
   */
  public function count(string $key, $value, float $sampleRate = 1.0, array $tags = []): void {
    $this->send($key, $value, 'c', $sampleRate, $tags);
  }

  /**
   * Sends a timing to statsd (in ms)
   *
   * @param string $key
   * @param float $value
   *   The timing in ms.
   * @param float $sampleRate
   * @param array $tags
   */
  public function timing(string $key, float $value, float $sampleRate = 1.0, array $tags = []): void {
    $this->send($key, $value, 'ms', $sampleRate, $tags);
  }

  /**
   * Sends a gauge, an arbitrary value to StatsD.
   *
   * @param string $key
   * @param string|int $value
   * @param array $tags
   */
  public function gauge(string $key, $value, array $tags = []): void {
    $this->send($key, $value, 'g', 1, $tags);
  }

  /**
   * Sends a set member.
   *
   * @param string $key
   * @param int $value
   * @param array $tags
   */
  public function set(string $key, int $value, array $tags = []): void {
    $this->send($key, $value, 's', 1, $tags);
  }

  /**
   * Actually sends a message to to the daemon and returns the sent message.
   *
   * @param string $key
   * @param int|float|string $value
   * @param string $type
   * @param float $sampleRate
   * @param array $tags
   */
  private function send(string $key, $value, string $type, float $sampleRate, array $tags = []): void {
    // Override sampleRate if all metrics should be sampled.
    if ($this->sampleRateAllMetrics < 1) {
      $sampleRate = $this->sampleRateAllMetrics;
    }

    if ($sampleRate < 1 && mt_rand() / mt_getrandmax() > $sampleRate) {
      return;
    }

    if (strlen($this->namespace) !== 0) {
      $key = $this->namespace . '.' . $key;
    }

    $message = $key . ':' . $value . '|' . $type;

    if ($sampleRate < 1) {
      $sampledData = $message . '|@' . $sampleRate;
    }
    else {
      $sampledData = $message;
    }

    try {
      $this->sendMessages([$sampledData]);
    }
    catch (\Exception $e) {
      // Ignore it: stats logging failure shouldn't stop the whole app.
    }

  }

  /**
   * Sends multiple messages to statsd.
   *
   * @param array $messages
   */
  public function sendMessages(array $messages): void {
    if (count($messages) === 0) {
      return;
    }

    if (!$this->isConnected()) {
      $this->connect($this->host, $this->port);
    }
    foreach ($messages as $message) {
      $this->writeToSocket($message);
    }
  }

  /**
   * Write message on socket.
   *
   * @param string $message
   *   Message.
   */
  protected function writeToSocket(string $message): void {
    if ($this->socket === NULL) {
      return;
    }

    // Suppress all errors.
    @fwrite($this->socket, $message);

    // Sleeping for 10 millionths of a second improves UDP reliability.
    usleep(10);
  }

  /**
   * Open connection to UDP socket.
   *
   * @param string $host
   *   Hostname.
   * @param int $port
   *   Port number.
   */
  protected function connect(string $host, int $port): void {
    $errorNumber = 0;
    $errorMessage = '';
    $url = 'udp://' . $host;

    $this->socket = @fsockopen($url, $port, $errorNumber, $errorMessage);

    $this->isConnected = TRUE;
  }

  /**
   * Checks whether the socket connection is alive.
   *
   * Only tries to connect once.
   *
   * ever after isConnected will return true,
   * because $this->socket is then false
   *
   * @return bool
   *   Connection status.
   */
  protected function isConnected(): bool {
    return $this->isConnected;
  }

  /**
   * Close the socket connection.
   */
  public function close(): void {
    if ($this->socket !== NULL) {
      fclose($this->socket);
    }

    $this->socket = NULL;
    $this->isConnected = FALSE;
  }

}
